"""Smopy: OpenStreetMap image tiles in Python.

Give a box in geographical coordinates (latitude/longitude) and a zoom level,
Smopy returns an OpenStreetMap tile image!

"""
# -----------------------------------------------------------------------------
# Imports
# -----------------------------------------------------------------------------
#from __future__ import print_function

from six import BytesIO
from urllib.request import urlopen

from PIL import Image #,  ImageDraw
import numpy as np
#import matplotlib.pyplot as plt
#from IPython.display import display_png
import time
import requests
import H5FileManager as H5FM
#import matplotlib.pylab as plb
# -----------------------------------------------------------------------------
# Constants
# -----------------------------------------------------------------------------
__version__ = '0.0.6'
TILE_SIZE = 256
MAXTILES = 20


# 20180820 C.Y. Peng, add, Hex to RGB
def hex_to_rgb(value):
    return tuple(int(value[i:i+2], 16) for i in (0, 2 ,4))

# 20180821 C.Y. Peng, add, for multilines
def MultiLinesMinMax(LatDict, LonDict):
    minLat = []
    minLon = []
    maxLat = []
    maxLon = []
    for idx in range(0, len(LatDict)):
        minLat.append(np.min(LatDict[idx]))
        minLon.append(np.min(LonDict[idx]))
        maxLat.append(np.max(LatDict[idx]))
        maxLon.append(np.max(LonDict[idx]))
    return minLat, minLon, maxLat, maxLon

def PlotMultiRoute(mapNo, Path, fileName, LatDict, LonDict, LineFormatDict, LeadstrDict):
    import smopy as sp
    if ((len(LatDict) > 0) and (len(LonDict) > 0)):
        minLat, minLon, maxLat, maxLon = sp.MultiLinesMinMax(LatDict, LonDict)
        # 20180822 C.Y. Peng, Bug Fixed
        # 20180823 C.Y. Peng, add time sleep for delay
        try:
            MapPlot = sp.Map(mapNo, (np.min(minLat), np.min(minLon), np.max(maxLat), np.max(maxLon)), z=20)
            MapPlot.save_multi_route_png(Path + fileName, LatDict, LonDict, LineFormatDict, LeadstrDict)
        except:
            print('Multi-Routes Plot Failed!')
            pass
        
        # 20180928 C.Y. Peng, add the image file to h5
        #IMG = np.array(IMG.getdata(),np.uint8).reshape(IMG.size[1], IMG.size[0], 3)
        #H5FM.Image2H5(IMG, Path + fileName, fileName+'_Image')
        #c = H5FM.H52Image(Path + fileName, fileName+'_Image')
        for idx in range(0, len(minLat)):
            try:
                MapPlot = sp.Map(mapNo, (minLat[idx], minLon[idx], maxLat[idx], maxLon[idx]), z=20)
                MapPlot.save_route_png(Path+LeadstrDict[idx], LatDict[idx], LonDict[idx], LineFormatDict[idx], LeadstrDict[idx])
            except:
                print('Route Plot Failed!')
                pass
        sp.SavePlot(Path, fileName)

# 20180928 C.Y. Peng, add the image file to h5
# 20181221 C.Y. Peng, Bug Fixed
def SavePlot(Path, fileName):
    import glob
    files_list = glob.glob(Path + '\\' +'*.png')
    for idx in range(0,len(files_list)):
        IMG = Image.open(files_list[idx])
        IMG_array = np.array(IMG.getdata(),np.uint8).reshape(IMG.size[1], IMG.size[0], 3)
        file_name_str = files_list[idx]
        file_name = file_name_str.split('\\')
        H5FM.Image2H5(IMG_array, Path + fileName, file_name[-1]) 
        # Read Image from Files
        #this_IMG = H5FM.H52Image(Path + fileName, file_name[-1])
        #img = Image.fromarray(this_IMG, 'RGB')
        #img.show()

# -----------------------------------------------------------------------------
# OSM functions
# -----------------------------------------------------------------------------
def get_url(x, y, z, tileserver):
    """Return the URL to the image tile (x, y) at zoom z."""
    return tileserver.format(z=z, x=x, y=y)


def fetch_tile(x, y, z, tileserver):
    """Fetch tile (x, y) at zoom level z from OpenStreetMap's servers.

    Return a PIL image.

    """
    # 2018/08/23 add the multi proxy, test ver
    """
    proxy_list = [
            '61.69.75.14:80',
            '123.160.31.71:8080',
            '115.231.128.79:8080',
            '166.111.77.32:80',
            '43.240.138.31:8080',
            '218.201.98.196:3128'
            ]
    my_headers = [
    "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36",
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36",
    "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:30.0) Gecko/20100101 Firefox/30.0",
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/537.75.14",
    "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Win64; x64; Trident/6.0)",
    'Mozilla/5.0 (Windows; U; Windows NT 5.1; it; rv:1.8.1.11) Gecko/20071127 Firefox/2.0.0.11',
   'Opera/9.25 (Windows NT 5.1; U; en)',
   'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)',
   'Mozilla/5.0 (compatible; Konqueror/3.5; Linux) KHTML/3.5.5 (like Gecko) (Kubuntu)',
   'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.12) Gecko/20070731 Ubuntu/dapper-security Firefox/1.5.0.12',
   'Lynx/2.8.5rel.1 libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/1.2.9',
   "Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Ubuntu/11.04 Chromium/16.0.912.77 Chrome/16.0.912.77 Safari/535.7",
   "Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:10.0) Gecko/20100101 Firefox/10.0 "
   req = urllib.request.Request(url)
    print(req)
    req.set_proxy(proxy_list[count], 'http')
    req.add_header('User-Agent', my_headers[count])
   ]
    
    done = 1
    count= 1
        while (done):
        req = urllib.request.Request(url)
        try:
            time.sleep(0.5)      
            req.set_proxy(proxy_list[count], 'http')
            png = BytesIO(urllib.request.urlopen(req).read())
            if (png):
                done = 0
        except:
            if (count == len(proxy_list)-1):
                done = 0
                print('Proxy Failed')
            count += 1
    """
    url  = get_url(x, y, z, tileserver)   
    time.sleep(0.5)      
    png = BytesIO(urlopen(url).read())
    img = Image.open(png)
    img.load()
    return img


def fetch_map(box, z, tileserver, tilesize, maxtiles):
    """Fetch OSM tiles composing a box at a given zoom level, and
    return the assembled PIL image."""
    box = correct_box(box, z)
    x0, y0, x1, y1 = box
    sx, sy = get_box_size(box)
    if sx * sy >= maxtiles:
        raise Exception(("You are requesting a very large map, beware of "
                         "OpenStreetMap tile usage policy "
                         "(http://wiki.openstreetmap.org/wiki/Tile_usage_policy)."))
    img = Image.new('RGB', (sx*tilesize, sy*tilesize))
    for x in range(x0, x1 + 1):
        for y in range(y0, y1 + 1):
            px, py = tilesize * (x - x0), tilesize * (y - y0)
            img.paste(fetch_tile(x, y, z, tileserver), (px, py))
    return img


def correct_box(box, z):
    """Get good box limits"""
    x0, y0, x1, y1 = box
    new_x0 = max(0, min(x0, x1))
    new_x1 = min(2**z - 1, max(x0, x1))
    new_y0 = max(0, min(y0, y1))
    new_y1 = min(2**z - 1, max(y0, y1))

    return (new_x0, new_y0, new_x1, new_y1)


def get_box_size(box):
    """Get box size"""
    x0, y0, x1, y1 = box
    sx = abs(x1 - x0) + 1
    sy = abs(y1 - y0) + 1
    return (sx, sy)


def determine_scale(latitude, z):
    """Determine the amount of meters per pixel

    :param latitude: latitude in radians
    :param z: zoom level

    Source: http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#Resolution_and_Scale

    """
    # For zoom = 0 at equator
    meter_per_pixel = 156543.03

    resolution = meter_per_pixel * np.cos(latitude) / (2 ** z)

    return resolution


# -----------------------------------------------------------------------------
# Utility imaging functions
# -----------------------------------------------------------------------------
def image_to_png(img):
    """Convert a PIL image to a PNG binary string."""
    exp = BytesIO()
    img.save(exp, format='png')
    exp.seek(0)
    s = exp.read()
    exp.close()
    return s


def image_to_numpy(img):
    """Convert a PIL image to a NumPy array."""
    return np.array(img)


# -----------------------------------------------------------------------------
# Functions related to coordinates
# -----------------------------------------------------------------------------
def deg2num(latitude, longitude, zoom, do_round=True):
    """Convert from latitude and longitude to tile numbers.

    If do_round is True, return integers. Otherwise, return floating point
    values.

    Source: http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#Python

    """
    lat_rad = np.radians(latitude)
    n = 2.0 ** zoom
    if do_round:
        f = np.floor
    else:
        f = lambda x: x
    xtile = f((longitude + 180.) / 360. * n)
    ytile = f((1.0 - np.log(np.tan(lat_rad) + (1 / np.cos(lat_rad))) / np.pi) /
              2. * n)
    if do_round:
        if isinstance(xtile, np.ndarray):
            xtile = xtile.astype(np.int32)
        else:
            xtile = int(xtile)
        if isinstance(ytile, np.ndarray):
            ytile = ytile.astype(np.int32)
        else:
            ytile = int(ytile)
    return (xtile, ytile)


def num2deg(xtile, ytile, zoom):
    """Convert from x and y tile numbers to latitude and longitude.

    Source: http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#Python

    """
    n = 2.0 ** zoom
    longitude = xtile / n * 360. - 180.
    latitude = np.degrees(np.arctan(np.sinh(np.pi * (1 - 2 * ytile / n))))

    return (latitude, longitude)


def get_tile_box(box_latlon, z):
    """Convert a box in geographical coordinates to a box in
    tile coordinates (integers), at a given zoom level.

    box_latlon is lat0, lon0, lat1, lon1.

    """
    lat0, lon0, lat1, lon1 = box_latlon
    x0, y0 = deg2num(lat0, lon0, z)
    x1, y1 = deg2num(lat1, lon1, z)
    return (x0, y0, x1, y1)


def get_tile_coords(lat, lon, z):
    """Convert geographical coordinates to tile coordinates (integers),
    at a given zoom level."""
    return deg2num(lat, lon, z, do_round=False)


def _box(*args):
    """Return a tuple (lat0, lon0, lat1, lon1) from a coordinate box that
    can be specified in multiple ways:

    A. box((lat0, lon0))  # nargs = 1
    B. box((lat0, lon0, lat1, lon1))  # nargs = 1
    C. box(lat0, lon0)  # nargs = 2
    D. box((lat0, lon0), (lat1, lon1))  # nargs = 2
    E. box(lat0, lon0, lat1, lon1)  # nargs = 4

    """
    nargs = len(args)
    assert nargs in (1, 2, 4)
    pos1 = None

    # Case A.
    if nargs == 1:
        assert hasattr(args[0], '__len__')
        pos = args[0]
        assert len(pos) in (2, 4)
        if len(pos) == 2:
            pos0 = pos
        elif len(pos) == 4:
            pos0 = pos[:2]
            pos1 = pos[2:]

    elif nargs == 2:
        # Case C.
        if not hasattr(args[0], '__len__'):
            pos0 = args[0], args[1]
        # Case D.
        else:
            pos0, pos1 = args[0], args[1]

    # Case E.
    elif nargs == 4:
        pos0 = args[0], args[1]
        pos1 = args[2], args[3]

    if pos1 is None:
        pos1 = pos0

    return (pos0[0], pos0[1], pos1[0], pos1[1])


def extend_box(box_latlon, margin=.1):
    """Extend a box in geographical coordinates with a relative margin."""
    (lat0, lon0, lat1, lon1) = box_latlon
    lat0, lat1 = min(lat0, lat1), max(lat0, lat1)
    lon0, lon1 = min(lon0, lon1), max(lon0, lon1)
    dlat = max((lat1 - lat0) * margin, 0.0005)
    dlon = max((lon1 - lon0) * margin, 0.0005 / np.cos(np.radians(lat0)))
    return (lat0 - dlat, lon0 - dlon,
            lat1 + dlat, lon1 + dlon)


# -----------------------------------------------------------------------------
# Main Map class
# -----------------------------------------------------------------------------
class Map(object):

    """Represent an OpenStreetMap image.

    Initialized as:

        map = Map((lat_min, lon_min, lat_max, lon_max), z=z, tileserver="")

    where the first argument is a box in geographical coordinates, and z
    is the zoom level (from minimum zoom 1 to maximum zoom 19).

    Methods:

    * To create a matplotlib plot: `ax = map.show_mpl()`.

    * To save a PNG: `map.save_png(filename)`.

    Tested tileservers:

    * http://tile.openstreetmap.org/{z}/{x}/{y}.png [default]

    * http://a.tile.stamen.com/toner/{z}/{x}/{y}.png [stamen toner (b/w high contrast)]

    * http://c.tile.stamen.com/watercolor/{z}/{x}/{y}.png [watercolor look]

    * https://tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png [grayscale]


    """

    def __init__(self, serveNo, *args, **kwargs):
        """Create and fetch the map with a given box in geographical
        coordinates.

        Can be called with `Map(box, z=z)` or `Map(lat, lon, z=z, tileserver="http://tile.openstreetmap.org/{z}/{x}/{y}.png")`.

        """
        z = kwargs.get('z', 18)
        margin = kwargs.get('margin', .05)
        
        # 2018/08/22 Add Map Lib
        mapLib    = []
        mapLib.append('https://mt0.google.com/vt?x={x}&y={y}&z={z}')
        mapLib.append('http://tile.openstreetmap.org/{z}/{x}/{y}.png')
        mapLib.append('https://tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png')
        self.tileserver = kwargs.get('tileserver', mapLib[serveNo])
        #self.tileserver = kwargs.get('tileserver', 'https://mt0.google.com/vt?x={x}&y={y}&z={z}')
        #self.tileserver = kwargs.get('tileserver', 'https://tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png')
        self.tilesize = kwargs.get('tilesize', 256)
        self.maxtiles = kwargs.get('maxtiles', 16)

        box = _box(*args)
        if margin is not None:
            box = extend_box(box, margin)
        self.box = box

        self.z = self.get_allowed_zoom(z)
        if z > self.z:
            print('Lowered zoom level to keep map size reasonable. '
                  '(z = %d)' % self.z)
        else:
            self.z = z
        self.box_tile = get_tile_box(self.box, self.z)

        # 20180821 add
        self.xmin = min(self.box_tile[0], self.box_tile[2])
        self.ymin = min(self.box_tile[1], self.box_tile[3])
        self.img = None
        self.fetch()

    def to_pixels(self, lat, lon=None):
        """Convert from geographical coordinates to pixels in the image."""
        return_2D = False
        if lon is None:
            if isinstance(lat, np.ndarray):
                assert lat.ndim == 2
                assert lat.shape[1] == 2
                lat, lon = lat.T
                return_2D = True
            else:
                lat, lon = lat
        x, y = get_tile_coords(lat, lon, self.z)
        px = (x - self.xmin) * TILE_SIZE
        py = (y - self.ymin) * TILE_SIZE
        if return_2D:
            return np.c_[px, py]
        else:
            return px, py

    def get_allowed_zoom(self, z=18):
        box_tile = get_tile_box(self.box, z)
        box = correct_box(box_tile, z)
        sx, sy = get_box_size(box)
        if sx * sy >= self.maxtiles:
            z = self.get_allowed_zoom(z - 1)
        return z

    def fetch(self):
        """Fetch the image from OSM's servers."""
        if self.img is None:
            self.img = fetch_map(self.box_tile, self.z, self.tileserver, self.tilesize, self.maxtiles)
        self.w, self.h = self.img.size
        return self.img

    def show_mpl(self, ax=None, figsize=None, dpi=None, **imshow_kwargs):
        """Show the image in matplotlib.

        Parameters
        ----------
        ax : matplotlib axes, optional
            Matplotlib axes used to show the image.  If `None`, a
            a new figure is created.  Default is `None`.
        figsize, dpi : optional
            These arguments are passed as arguments for the created
            figure if `ax` is `None`.  Default is `None` for both
            parameters.
        **imshow_kwargs : optional
            All remaining keyword arguments are passed to matplotlib
            imshow.
        """
        if not ax:
            plt.figure(figsize=figsize, dpi=dpi)
            ax = plt.subplot(111)
            plt.xticks([])
            plt.yticks([])
            plt.grid(False)
            plt.xlim(0, self.w)
            plt.ylim(self.h, 0)
            plt.axis('off')
            plt.tight_layout()
        plt.imshow(self.img, **imshow_kwargs)
        return ax
    
    def show_ipython(self):
        """Show the image in IPython as a PNG image."""
        png = image_to_png(self.img)
        display_png(png, raw=True)

    def to_pil(self):
        """Return the PIL image."""
        return self.img

    def to_numpy(self):
        """Return the image as a NumPy array."""
        return image_to_numpy(self.img)

    def save_png(self, filename):
        """Save the image to a PNG file."""
        png = image_to_png(self.img)
        with open(filename, 'wb') as f:
            f.write(png)
  
    # 20180821 C.Y. Peng, add, Multi Route Plot
    def save_multi_route_png(self, filename, LatDict, LonDict, LineFormatDict, LeadstrDict):
        for idx in range(0, len(LatDict)):
            img = self.DotPlot(LatDict[idx], LonDict[idx], LineFormatDict[idx], LeadstrDict[idx])
        png = image_to_png(img)
        with open(filename+'.png', 'wb') as f:
            f.write(png)
        return img
        
    
    def save_route_png(self, filename, lat, lon, NoFormat, Leadstr):
        img = self.DotPlot(lat, lon, NoFormat, Leadstr)
        png = image_to_png(img)
        with open(filename+'.png', 'wb') as f:
            f.write(png)

    def DotPlot(self, lat, lon, NoFormat, title):
        #draw = ImageDraw.Draw(self.img)
        img  = self.img
        #width, height= img.size
        RGB, IconHref, EndIconHref  = self.LineFormat(int(NoFormat))
        file = requests.get('http://maps.google.com/mapfiles/kml/paddle/'+title[0]+'.png')
        iImg = Image.open(BytesIO(file.content))
        iImgRes = iImg.resize((40, 40))
        iw, ih = iImgRes.size
        file = requests.get(IconHref)
        rImg = Image.open(BytesIO(file.content))
        rImgRes = rImg.resize((10, 10))
        file = requests.get(EndIconHref)
        eImg = Image.open(BytesIO(file.content))
        eImgRes = eImg.resize((40, 40))
        ew, eh = eImgRes.size
        for idx in range(0, len(lat)):
            a, b = self.to_pixels(lat[idx], lon[idx])
            #draw.ellipse((a-1, b-1, a+1, b+1), fill = RGB)
            if (idx == 0):
                img.paste(iImgRes, (int(a)-int(iw*0.5), int(b)-ih), iImgRes) 
            if (idx == len(lat)-1):
                img.paste(eImgRes, (int(a), int(b)-eh), eImgRes)
            img.paste(rImgRes,(int(a),int(b)),rImgRes)
        return img

    def LineFormat(self, NoFormat):  
        NoFormat = NoFormat%10
        # Red
        if (NoFormat == 1):
           ColorCode = 'e74c3c'

        # Blue
        if (NoFormat == 2):
           ColorCode = '2980b9'
           
        # Green
        if (NoFormat == 3):
           ColorCode= '27ae60'
           
        # Orange
        if (NoFormat == 4):
           ColorCode= 'f39c12'
           
        # black
        if (NoFormat == 5):
           ColorCode= '000000'
 
        # yellow
        if (NoFormat == 6):
           ColorCode= 'f1c40f'
        
        # purple
        if (NoFormat == 7):
           ColorCode= '9b59b6'
        
        # grey 
        if (NoFormat == 8):
           ColorCode= 'bdc3c7'
            
        # gray 
        if (NoFormat == 9): 
           ColorCode = '7f8c8d'
           
        LineColor= hex_to_rgb(ColorCode)
        IconHref = "https://png.icons8.com/ios-glyphs/50/" +ColorCode+ '/filled-circle.png'
        EndIconHref = "https://png.icons8.com/ios-glyphs/40/" +ColorCode+ '/pin3.png'
        return LineColor, IconHref, EndIconHref  
   
