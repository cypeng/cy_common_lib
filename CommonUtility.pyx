# -*- coding: utf-8 -*-
"""
Created on Fri Jul  6 17:37:18 2018

@author: C.Y. Peng

DataVer  - Author -  Note
20180706 - C.Y. Peng - First Ver. Release
20190508 - C.Y. Peng - Algorithm Update
20191129 - C.Y. Peng - add the get_title, some path representation update
"""
import numpy as np
cimport numpy as np
import math
import os
import re
"""
cpdef AutoCatchFilePath(KeyFileName):
    here = os.path.dirname(os.path.realpath(__file__))
    FilePathList = []
    FileNameList = []
    FileClassificationList = []
    for allfileroot in os.walk(here):
        fileStrList = allfileroot[2]
        for idx in range(0,len(fileStrList)):
            jdx = -1*len(FileNameExtension)
            if (KeyFileName in fileStrList[idx]):
                FilePathList.append(allfileroot[0]+'\\')
                FileNameList.append(fileStrList[idx][:jdx])
                fileStrArray = fileStrList[idx][:jdx].split('_')
                FileClassificationList.append(fileStrArray[-1])

    return FilePathList, FileNameList, FileClassificationList
"""

def get_title(name):
    title_search = re.search(' ([A-Za-z]+)\.', name)
    # If the title exists, extract and return it.
    if title_search:
        return title_search.group(1)
    return ""

def createOutputFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print ('Error: Creating directory. ' +  directory)

def checkPathExist(filepath):
    if os.path.exists(filepath):
       return bool(1)
    return bool(0)

def updateOutputFolder(folderPath, outputfolder = "result_"):
    num = 0
    dest_output_path = os.path.join(folderPath, outputfolder + str(num))
    while (checkPathExist(dest_output_path)):
        num +=1
        dest_output_path = os.path.join(folderPath, outputfolder + str(num))
    return dest_output_path

cdef class MeanVarianceAnalysis:
    cpdef float mean, variance, std
    cpdef int count
    
    def __init__(self):
        self.count = 0
        self.mean  = 0
        self.std   = 0
        self.variance = 0
    
    def __cinit__(self):
        self.count = 0
        self.mean  = 0
        self.std   = 0
        self.variance = 0
    
    cpdef resetAlg(self):
        self.count = 0
        self.mean  = 0
        self.std   = 0
        self.variance = 0
    
    cpdef iterativeMeanVariance(self, np.float data):
        cpdef np.float delta, delta2
        
        self.count += +1;
        delta = data - self.mean
        self.mean = self.mean + delta/self.count
        delta2 = data - self.mean
        self.variance += delta*delta2
        if (self.count > 1):
            self.std = math.sqrt(self.variance/(self.count-1))
    
    cpdef np.float getMean(self):
        return  self.mean
