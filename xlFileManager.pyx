# -*- coding: utf-8 -*-
"""
Created on Mon Sep 10 18:32:42 2018

@author: user009

DataVer  - Author - Note
20180911 - C.Y. Peng - First Ver. Release
20181105 - C.Y. Peng - xlsx Extension Name Check
"""
from openpyxl import load_workbook as lwb
from openpyxl import Workbook as wb
import numpy as np
cimport numpy as np

cpdef Data2xl(np.ndarray data_to_write, str fileName, str datasetName):
    cpdef int om, on, m, n
    xlsxFileName = __isPickleExtensionName__(fileName)
    
    try:
        f = lwb(xlsxFileName)    
    except:
        f = wb()
        pass
    
    try:
        sheet = f[datasetName]
    except:
        sheet = f.create_sheet(title = datasetName)
        pass
    om = sheet.max_row
    on = sheet.max_column
    if ((1 == om) and (1 == on)):
        if (sheet.cell(om, on).value):
            om+=1
    else:
        om+=1
    m, n = np.shape(data_to_write)
    for idx in range(0,m):
        for jdx in range(0,n):
            sheet.cell(om +idx, jdx+1).value = data_to_write[idx][jdx]
    f.save(xlsxFileName)
    f.close()
    
cpdef xl2Data(str fileName, str datasetName):
    cpdef list data
    cpdef bint typeflag
    cpdef int m, n
    xlsxFileName = __isPickleExtensionName__(fileName)
    
    try:
        f = lwb(xlsxFileName) 
        sheet = f[datasetName]
    except:
        data = [0]
        pass
    
    m = sheet.max_row
    n = sheet.max_column
    data = [None]*m
    for idx in range(0,m):
        list_data = [None]*n
        for jdx in range(0,n):
            str_value = str(sheet.cell(idx+1, jdx+1).value)
            if (str_value.isdigit()):
                typeflag = 0
                list_data[jdx] = float(str_value)
            else:
                typeflag = 1
                list_data[jdx] = str_value
        if (typeflag):
            data[idx] = np.array(list_data, dtype = object)
        else:
            data[idx] = np.array(list_data)
    
    f.close()  
    return data

cpdef __isPickleExtensionName__(str fileName):
    if (fileName.endswith('.xlsx')):
        return fileName
    else:
        return fileName + '.xlsx'