# -*- coding: utf-8 -*-
"""
Created on Mon Jun 25 17:13:07 2018

@author: user009

DataVer  - Author - Note
20180418 - C.Y. Peng - To add the all kml file in one kml file (Test)
                       To test simplekml library
20180426 - C.Y. Peng - Modulize the Functions
                       Add kml Lib
20180504 - C.Y. Peng - Change the Color of the Line
                       Add points: start, end, and the way
20180514 - C.Y. Peng - Add KML Multi-Lines Function
                       Lines Format Function
20180514-2 - C.Y. Peng - Add the Package KML File
20180627 - C.Y. Peng - KML Function Modified - General Ver. Release
                       Add H5 File Function
                       Add Pickle File Function
20180628 - C.Y. Peng - H5 File Save Bug Fixed
20180629 - C.Y. Peng - Divided into Three Files
20181105 - C.Y. Peng - Pickle Extension Name Check
"""
import pickle
import numpy as np
cimport numpy as np

# pickle format
cpdef Data2Pickle(np.ndarray data_to_write, str fileName):
    PickleFileName = __isPickleExtensionName__(fileName)
    
    f = open(PickleFileName, 'wb')
    pickle.dump(data_to_write, f)
    f.close()

cpdef Pickle2Data(str fileName):
    cpdef np.ndarray data
    PickleFileName = __isPickleExtensionName__(fileName)
    
    f = open(PickleFileName, 'rb')
    data = pickle.load(f)
    return data    

cpdef __isPickleExtensionName__(str fileName):
    if (fileName.endswith('.pckl')):
        return fileName
    else:
        return fileName + '.pckl'