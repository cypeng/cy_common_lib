# -*- coding: utf-8 -*-
"""
@author: CY Peng

DataVer  -    Author - Note
2019/05/20      CY     Test OK
2019/05/28      CY     Add Float Type
2019/06/30      CY     First Release
"""

import tensorflow as tf
import os
#import matplotlib.image as mpimg
import shutil
import numpy as np

def __is_png_image__(img_path):
    ext = os.path.splitext(img_path)[1].lower()
    return ext == '.png'

# Run graph to convert PNG image data to JPEG data
def __convert_png_to_jpeg__(img):
    tf.reset_default_graph()
    png_enc = tf.image.decode_png(img, channels=3)
    png_to_jpeg = tf.image.encode_jpeg(png_enc, format='rgb', quality=100)
    #sess = tf.get_default_session()
    return png_to_jpeg

def __isTFRecordName__(fileName):
    if (fileName.endswith('.tfrecords')):
        return fileName
    else:
        return fileName + '.tfrecords'

def int64_feature(value):
  return tf.train.Feature(int64_list = tf.train.Int64List(value=[value]))

def bytes_feature(value):
    return tf.train.Feature(bytes_list = tf.train.BytesList(value=[value]))

def floats_feature(value):
    return tf.train.Feature(float_list=tf.train.FloatList(value=value))

def convert_multiple_images(tfrecord_file_name, read_num, image_tfrecord_function, *function_args):
    save_filename = __isTFRecordName__(tfrecord_file_name)
    #print(*function_args)

    with tf.python_io.TFRecordWriter(save_filename) as writer:
        for idx in range(read_num):
            image_info = image_tfrecord_function(bytes(str(idx), encoding='utf-8'),
                                                 idx,
                                                 *function_args)

            writer.write(image_info.SerializeToString())
    print("Save Files Completed!")

def one_shot_extract_multiple_images(tfrecord_image_function,
                                     tfrecord_filename,
                                     shuffle_size,
                                     repeat_num,
                                     batch_size):
    read_filename = __isTFRecordName__(tfrecord_filename)
    dataset = tf.data.TFRecordDataset(read_filename)
    dataset = dataset.map(tfrecord_image_function).\
                      shuffle(shuffle_size).\
                      batch(batch_size).\
                      repeat(repeat_num)

    iterator = dataset.make_one_shot_iterator()

    return iterator.get_next()

"""
Old Version: Will be Removed from the New Lib
capacity: An integer. The maximum number of elements in the queue.
capacity=(min_after_dequeue+(num_threads+a small safety margin∗batchsize)
min_after_dequeue: Minimum number elements in the queue after a
dequeue(出列), used to ensure a level of mixing of elements.
"""
def extract_multiple_images(tfrecord_image_function,
                            tfrecord_filename,
                            epoch_num,
                            threads_num,
                            capacity_num,
                            batch_size,
                            min_after_dequeue_num):

    read_filename = __isTFRecordName__(tfrecord_filename)
    filename_queue = tf.train.string_input_producer([read_filename], num_epochs= epoch_num)
    reader = tf.TFRecordReader()
    _, serialized_example = reader.read(filename_queue)

    return tf.train.shuffle_batch(tfrecord_image_function(serialized_example),
                                  batch_size = batch_size,
                                  num_threads = threads_num,
                                  capacity = capacity_num,
                                  min_after_dequeue = min_after_dequeue_num)