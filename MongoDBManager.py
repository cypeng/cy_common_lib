# -*- coding: utf-8 -*-
"""

@author: user009

Ref.
https://juejin.im/post/5addbd0e518825671f2f62ee
http://api.mongodb.com/python/current/examples/authentication.html#def
http://blog.51cto.com/xitongjiagoushi/1657096

Example 1
    d = {'name': ['Braund', 'Cummings', 'Heikkinen', 'Allen'],
         'age': [22, 38, 26, 35],
         'fare': [7.25, 71.83, 0, 8.05],
         'survived?': [False, True, True, False]}
    print(type(d))
    data = pd.DataFrame(d)
    dataframe_to_mongo(client, database_name, 'test3', data)

    Example 2
    d = {'name': ['Braund', 'Cummings', 'Heikkinen', 'Allen'],
         'age': [22, 38, 26, 35],
         'fare': [7.25, 71.83, 0, 8.05],
         'survived?': [False, True, True, False]}

    datadict_to_mongo(client, database_name, 'test3', d)

    Example 3
    data = mongo_to_dataframe(client, database_name, 'test2')

    Example 4
    data = mongo_to_datalist(client, database_name, 'test2')
    print(data)

    Example 5
    d = {'name': ['Braund', 'Cummings', 'Heikkinen', 'Allen'],
         'age': [22, 38, 26, 35],
         'fare': [7.25, 71.83, 0, 8.05],
         'survived?': [False, True, True, False]}
    print(type(d))
    data = pd.DataFrame(d)
    dataframe_to_mongo_json(client, database_name, 'test4', data)

    Example 6
    data_dict = {
        "owner_name": "samssmilin",
        "photo_id": "602880671",
        "tags": "",
        "longitude": "-121.106479",
        "height": "766",
        "datetaken": "2004-01-17 21:05:35",
        "width": "1024",
        "length": 38141,
        "photo_title": "Dad and Elijah",
        "latitude": "35.565222",
        "photo_url": "https://farm2.staticflickr.com/1063/602880671_c2f4511ef4_b.jpg",
        "dateupload": "1075355967",
        "owner_id": "45365637@N00"
    }

    gridfs_to_mongo(client, database_name, 'test1', data_dict)

    Example 7
    data_dict = {
        "file": "slogOut_20181212.txt",
    }
    gridfs_to_mongo(client, database_name, 'text_table_2', "slogOut_20181212.txt", data_dict)

    Example 8
    mongo_to_gridfs(client, database_name, 'text_table_2', 'test.txt')
    mongo_to_gridfs(client, database_name, 'text_table', 'test.png')

    Exmaple 9
       data_dict = {
        "file": "test.h5",
     }
     gridfs_to_mongo(client, database_name, 'HDF5_TEST', "test.h5", data_dict)
     mongo_to_gridfs(client, database_name, 'HDF5_TEST', "test1.h5")

DataVer  - Author - Note
20190124   CY       Upload/ Download Data - Test OK
20190129   CY       Bug Fixed for uploading data
20190211   CY       Bug Fixed for uploading data for gridFS
20190215   CY       Bug Fixed for the Warning!/ Log Info Update
"""
import pymongo as pm
from easygui import passwordbox, fileopenbox
import codecs
import pandas as pd
#import json
import gridfs
import requests
import csv
from bson.objectid import ObjectId #這東西再透過ObjectID去尋找的時候會用到

def GetVer():
    DataVerName = '20190215'
    return DataVerName

def __log_info__():
    print("Select the Sign In Information File for Logging in to the MongoDB!")
    file_name = fileopenbox("Select the Sign In Information File:")
    try:
        log_file = codecs.open(file_name)
        tag_done = bool(0)
    except:
        tag_done = bool(1)
        pass
    count = 1
    url = None
    while (not tag_done):
        base_line = log_file.readline().strip("\n")
        #base_line = base_all.split(',')
        if (count == 2):
            # 20180831 add the warning
            try:
                url = base_line
            except:
                print("Wrong Format in Sing In Information File, Please Check!")
                pass

        count += 1
        if (count == 3):
            tag_done = bool(1)
            log_file.close()

    return url

def mongoDB_login():
    URL = __log_info__()

    if (URL == None):
        return None, None
    username = passwordbox("Username/ Email?\n")
    password = passwordbox("Password?\n")
    if (username == None) and (password== None):
        return None, None

    URL = URL.replace('<dbuser>',username)
    URL = URL.replace('<dbpassword>', password)
    DatabaseName_array = URL.split("/")
    DatabaseName = DatabaseName_array[-1]
    # mongodb://<user_name>:<user_password>@ds<xxxxxx>.mlab.com:<xxxxx>/<database_name>，請務必既的把腳括號的內容代換成自己的資料。
    # mongodb://<dbuser>:<dbpassword>@ds159574.mlab.com:59574/cloud_mongodb

    return pm.MongoClient(URL), DatabaseName

"""
def dataframe_to_mongo_json(client, database_name, table_name, dataframe):
    database = client[database_name]  # SQL: Database Name
    collection = database[table_name]  # SQL: Table Name

    records = dataframe.T.to_json()  # 參數 record 代表把列轉成個別物件
    collection.insert_many(json.loads(records).values())
    print("Upload the Data Completed!")
"""

def dataframe_to_mongo(client, database_name, collection_name, dataframe):
    database = client[database_name]  # SQL: Database Name
    collection = database[collection_name]   # SQL: Table Name

    records = dataframe.to_dict('records') # 參數 record 代表把列轉成個別物件
    if (collection_name in database.collection_names()):
        collection.delete_many(records)
        collection.insert_many(records)
    else:
        collection.insert_many(records)
    #collection.insert_many(records)
    print("Upload the Data to the MongoDB: Completed!")

def datadict_to_mongo(client, database_name, collection_name, datadict):
    database = client[database_name]  # SQL: Database Name
    collection = database[collection_name]   # SQL: Table Name

    #records = datadict.to_dict('records') # 參數 record 代表把列轉成個別物件
    if (collection_name in database.collection_names()):
        collection.delete_one(datadict)
        collection.insert_one(datadict)
    else:
        collection.insert_one(datadict)
    print("Upload the Data to the MongoDB: Completed!")

def mongo_to_dataframe(client, database_name, collection_name):
    database = client[database_name]  # SQL: Database Name
    collection = database[collection_name]  # SQL: Table Name
    dataframe = pd.DataFrame(list(collection.find()))
    print("Download the Data to the MongoDB: Completed!")
    return dataframe

def mongo_to_datadict(client, database_name, collection_name):
    database = client[database_name]  # SQL: Database Name
    collection = database[collection_name]  # SQL: Table Name
    datadict = list(collection.find())
    print("Download the Data to the MongoDB: Completed!")
    return datadict[0]

def gridfs_to_mongo(client, database_name, collection_name, filename, data_dict):
    database = client[database_name]  # SQL: Database Name
    if (collection_name + ".chunks" in database.collection_names()) and (collection_name + ".files" in database.collection_names()):
        collection = database[collection_name + ".chunks"]
        collection.delete_many({})
        collection = database[collection_name + ".files"]
        collection.delete_many({})

    FS = gridfs.GridFS(database, collection=collection_name)
    with open(filename, 'rb') as dictionary:
        FS.put(dictionary, **data_dict)

    # Web Picture
    # data = requests.get(data_dict["file"], timeout=10).content
    #if not FS.find_one({"file": data_dict["file"]}):
    #    FS.put(data, **data_dict)
    print("Upload the File to the MongoDB: Completed!")

def mongo_to_gridfs(client, database_name, collection_name, filename):
    database = client[database_name]  # SQL: Database Name

    FS = gridfs.GridFS(database, collection = collection_name)

    for grid_out in FS.find(no_cursor_timeout = True):
        #print(grid_out)
        data = grid_out.read()
        outf = open(filename, 'wb')
        outf.write(data)
        outf.close()

    print("Download the File from the MongoDB: Completed!")

def close_mongo(client):
    client.close()

"""
def main():
    # connection
    client, database_name = mongoDB_login()
    # 如果你只想連本機端的server你可以忽略，遠端的url填入:
    # mongodb://<user_name>:<user_password>@ds<xxxxxx>.mlab.com:<xxxxx>/<database_name>，
    # 請務必既的把腳括號的內容代換成自己的資料。
    #gridfs_to_mongo(client, database_name, 'txt_test', 'slogOut_20181212.txt', {})
    mongo_to_gridfs(client, database_name, 'txt_test', 'test.txt')
    close_mongo(client)

    # test if connection success
    #collection.stats  # 如果沒有error，你就連線成功了。

if __name__== "__main__":
    main()
"""
