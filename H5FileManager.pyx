# -*- coding: utf-8 -*-
"""
Created on Mon Jun 25 17:13:07 2018

@author: user009

DataVer  - Author - Note
20180418 - C.Y. Peng - To add the all kml file in one kml file (Test)
                       To test simplekml library
20180426 - C.Y. Peng - Modulize the Functions
                       Add kml Lib
20180504 - C.Y. Peng - Change the Color of the Line
                       Add points: start, end, and the way
20180514 - C.Y. Peng - Add KML Multi-Lines Function
                       Lines Format Function
20180514-2 - C.Y. Peng - Add the Package KML File
20180627 - C.Y. Peng - KML Function Modified - General Ver. Release
                       Add H5 File Function
                       Add Pickle File Function
20180628 - C.Y. Peng - H5 File Save Bug Fixed
20180629 - C.Y. Peng - Divided into Three Files, bug fixed
20180831 - C.Y. Peng - H5 File String Date Writing/Reading
20180928 - C.Y. Peng - Add the group name
20181005 - C.Y. Peng - Add the data type of the matrix
20181105 - C.Y. Peng - Re-Save H5 File, Extension Name Check 
20200102   C.Y. Peng - Data Re-Save for Each Function
"""
import h5py
import numpy as np
cimport numpy as np
# 20180928 C.Y. Peng, Picture Process of HDF File
import glob
import os

# h5 format 
cpdef Data2H5(np.ndarray data_to_write, str fileName, str datasetName, str groupName = None, np.dtype dtype = None):
    H5fileName = __isH5ExtensionName__(fileName)
    
    # 2018/09/28 C.Y. Peng, Group Data Add
    file = h5py.File(H5fileName,'a')   
    if (groupName):
        group = file.get(groupName)
        if (None == group):
            group = file.create_group(groupName)
    else:
        group = file
    
        
    try:
        #　20180628 - C.Y. Peng - H5 File Save Bug Fixed
        del group[datasetName]
        group[datasetName] = data_to_write    
    except:
        group.create_dataset(datasetName, data = data_to_write, dtype = dtype)
        pass
    file.close()
    H5ReSave(fileName)

cpdef np.ndarray H52Data(str fileName, str datasetName, str groupName = None, np.dtype dtype = None):
    cpdef np.ndarray data
    H5fileName = __isH5ExtensionName__(fileName)
    
    try:
        # 20180629 - C.Y. Peng - H5 File Bug Fixed
        # 20180928 - C.Y. Peng, Group Data Add   
        with h5py.File(H5fileName,'r') as file:
            if (None == groupName):
                group = file
            else:
                group = file.get(groupName)
            if (None == dtype):
                data = np.array(group[datasetName][:])
            else:
                data = np.array(group[datasetName][:], dtype = dtype)
    except:
        data = np.zeros((1,1))
        print("Wrong H5 File Reading for {}, {}".format(datasetName, fileName))
        pass
    
    return data 

# 2018/08/31 - C.Y. Peng - H5 File String Date Writing/Reading
cpdef  StringArray2H5(np.ndarray string_to_write, str fileName, str datasetName, str groupName = None):  
    H5fileName = __isH5ExtensionName__(fileName)
    
    # 2018/09/28 C.Y. Peng, Group Data Add
    file = h5py.File(H5fileName,'a')   
    h5type = h5py.special_dtype(vlen = str)
    if (groupName):
        group = file.get(groupName)
        if (None == group):
            group = file.create_group(groupName)
    else:
        group = file
     
    try:
        del group[datasetName]
        group.create_dataset(datasetName,(len(string_to_write),), dtype = h5type)
        group[datasetName][:] = string_to_write 
    except:
        group.create_dataset(datasetName,(len(string_to_write),), dtype = h5type)
        group[datasetName][:] = string_to_write  
        pass
    file.close()
    H5ReSave(fileName)
     
cpdef np.ndarray H52StringArray(str fileName, str datasetName, str groupName = None):
    cpdef np.ndarray data
    H5fileName = __isH5ExtensionName__(fileName)
    
    try:
        # 20180928 C.Y. Peng, Group Data Add    
        with h5py.File(H5fileName,'r') as file:
            if (None == groupName):
                group = file
            else:
                group = file.get(groupName) 
            data = group[datasetName][:]
    except:
        data = np.array(['' for _ in range(1)], dtype = object)
        print("Wrong H5 File Reading")
        pass
    
    return data 

# 2018/09/28 - C.Y. Peng - H5 File Image Writing/Reading
cpdef Image2H5(np.ndarray data_to_write, str fileName, str datasetName, str groupName = None):
    H5fileName = __isH5ExtensionName__(fileName)
    
    file = h5py.File(H5fileName,'a')   
    if (groupName):
        group = file.get(groupName)
        if (None == group):
            group = file.create_group(groupName)
    else:
        group = file
    
    dset = group.create_dataset(name = datasetName, data = data_to_write)
    dset.attrs['CLASS'] = 'IMAGE'
    dset.attrs['IMAGE_VERSION'] = '1.2'
    dset.attrs['IMAGE_SUBCLASS'] = 'IMAGE_TRUECOLOR'
    dset.attrs['INTERLACE_MODE'] = 'INTERLACE_PIXEL'
    file.close()
    H5ReSave(fileName)

cpdef np.ndarray H52Image(str fileName, str datasetName, str groupName = None):   
    cpdef np.ndarray data
    H5fileName = __isH5ExtensionName__(fileName)
    
    try:
        # 20180928 C.Y. Peng, Group Data Add    
        with h5py.File(H5fileName,'r') as file:
            if (None == groupName):
                group = file
            else:
                group = file.get(groupName) 
            data = group[datasetName][:]
    except:
        data = group[datasetName][:]
        print("Wrong H5 File Reading")
        pass
    
    return data 

# 20191203 C.Y. Peng, Find Datasets H5 File
cpdef list H5datasets(str fileName, str groupName = None):
    H5fileName = __isH5ExtensionName__(fileName)
    try: 
        with h5py.File(H5fileName,'r') as file:
            if (None == groupName):
                group = file
            else:
                group = file.get(groupName)
            datasets_names = list(group.keys())
    except:
        datasets_names = list(group.keys())
        print("Wrong H5 File Reading")
        pass
    file.close()
    return datasets_names

# 20181105 C.Y. Peng, Re-Save H5 File 
cpdef H5ReSave(str fileName):
    H5fileName = __isH5ExtensionName__(fileName)
    
    fs = h5py.File(H5fileName, 'r')
    fd = h5py.File('temp.h5', 'w')
    for name in fs:
        fs.copy(name, fd)
    fs.close()
    fd.close()
    os.remove(H5fileName)
    os.rename('temp.h5',H5fileName)
    
cpdef __isH5ExtensionName__(str fileName):
    if (fileName.endswith('.h5')):
        return fileName
    else:
        return fileName + '.h5'