# -*- coding: utf-8 -*-
"""
Created on Mon Jun 25 17:13:07 2018

@author: user009

DataVer  - Author - Note
20180418 - C.Y. Peng - To add the all kml file in one kml file (Test)
                       To test simplekml library
20180426 - C.Y. Peng - Modulize the Functions
                       Add kml Lib
20180504 - C.Y. Peng - Change the Color of the Line
                       Add points: start, end, and the way
20180514 - C.Y. Peng - Add KML Multi-Lines Function
                       Lines Format Function
20180514-2 - C.Y. Peng - Add the Package KML File
20180627 - C.Y. Peng - KML Function Modified - General Ver. Release
                       Add H5 File Function
                       Add Pickle File Function
20180628 - C.Y. Peng - H5 File Save Bug Fixed
20180629 - C.Y. Peng - Divided into Three Files
"""
import simplekml
import numpy as np
cimport numpy as np

# 20180514, Lines Format Function C.Y. Peng        
def kmlFormat(int NoFormat):
    cpdef str LineColor
    cpdef str ColorCode
    
    NoFormat = NoFormat%10
    # Red
    if (NoFormat == 1):
        LineColor= simplekml.Color.red
        ColorCode = 'e74c3c' 

        
    # Blue
    if (NoFormat == 2):
        LineColor= simplekml.Color.blue
        ColorCode = '2980b9'
        
    # Green
    if (NoFormat == 3):
        LineColor= simplekml.Color.green
        ColorCode= '27ae60'
        
    # Orange
    if (NoFormat == 4):
        LineColor= simplekml.Color.orange
        ColorCode= 'f39c12'
        
    # black
    if (NoFormat == 5):
        LineColor= simplekml.Color.black
        ColorCode= '000000'
 
    # yellow
    if (NoFormat == 6):
        LineColor= simplekml.Color.yellow
        ColorCode= 'f1c40f'
        
    # purple
    if (NoFormat == 7):
        LineColor= simplekml.Color.purple
        ColorCode= '9b59b6'
        
    # grey 
    if (NoFormat == 8):
        LineColor= simplekml.Color.grey 
        ColorCode= 'bdc3c7'
            
    # gray 
    if (NoFormat == 9):
        LineColor= simplekml.Color.gray 
        ColorCode = '7f8c8d'
        
    IconHref = "https://png.icons8.com/ios-glyphs/50/" +ColorCode+ '/filled-circle.png'
    EndIconHref = "https://png.icons8.com/ios-glyphs/40/" +ColorCode+ '/pin3.png'
    return LineColor, IconHref, EndIconHref  

# 20180514, KML Multi-Lines Function C.Y. Peng
cpdef File2KMLforMultiLines(str path, str fileName, list LatDict, list LonDict, list LineFormatDict, list leadstrDict):
    # 20180514 C.Y. Peng, add the line points
    # Ref.
    # https://icons8.com/icon/new-icons/all
    cpdef np.ndarray Lat, Lon
    cpdef int FormatCount, lsInitial
    cpdef str leadstr

    kml = simplekml.Kml(name = fileName)
    # 20180514 Folder/ Document Test
    doc = kml.newdocument(name = fileName+'Document')     
    # print(idx,self.TagBase[idx][1])
    # 20180504 add the start and end points
    # 20180606 Data Scale
    # 20180627 MultiLines
    for idx in range(0, len(LatDict)):
        Lat = LatDict[idx]
        Lon = LonDict[idx]
        FormatCount = int(LineFormatDict[idx])
        leadstr = str(leadstrDict[idx])
        
        # 20180514 Folder/ Document Test  
        fol = doc.newdocument(name = leadstr)
        ls0 = fol.newlinestring(name = leadstr+'-line')
        ls1 = fol.newpoint(name=leadstr+'-start')
        ls2 = fol.newpoint(name=leadstr+'-end')
        ls3 = fol.newmultigeometry(name=leadstr+'-points')
        
        ls0.style.linestyle.width = 0.5
        ls1.style.iconstyle.scale = 3  # Icon thrice as big
        ls2.style.iconstyle.scale = 3  # Icon thrice as big      
        ls3.style.iconstyle.scale = 0.3 
        
        ls1.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/paddle/'+leadstr[0]+'.png'
        ls0.style.linestyle.color, ls3.style.iconstyle.icon.href, ls2.style.iconstyle.icon.href = kmlFormat(FormatCount)
        ls0.coords = []
        ls1.coords = []
        ls2.coords = []
        ls3.coords = []
        
        lsInitial = 1
        for idx in range(0, len(Lat)):
            ls0.coords.addcoordinates([(Lat[idx],Lon[idx])])
            ls2.coords = [(Lat[idx],Lon[idx])]
            ls3.newpoint(coords= [(Lat[idx],Lon[idx])])
            if (lsInitial == 1):
                lsInitial = 0
                ls1.coords.addcoordinates([(Lat[idx],Lon[idx])])
    kml.save(path+'/'+fileName+'.kml')
                
# 20180514, KML Multi-Lines Function (Package) C.Y. Peng
cpdef File2KMLforMultiLinesPackage(str path, str fileName, list LatDict, list LonDict, list LineFormatDict, list leadstrDict):
    # 20180514 C.Y. Peng, add the line points
    # Ref.
    # https://icons8.com/icon/new-icons/all 
    cpdef np.ndarray Lat, Lon
    cpdef int FormatCount, lsInitial
    cpdef str leadstr
    
    kml = simplekml.Kml(name = fileName)
    for idx in range(0, len(LatDict)):
        # 20180504 add the start and end points
        # 20180606 Data Scale
        # 20180627 MultiLines        
        Lat = LatDict[idx]
        Lon = LonDict[idx]
        FormatCount = int(LineFormatDict[idx])
        leadstr = str(leadstrDict[idx])
        
        # 20180514 Folder/ Document Test  
        ls0 = kml.newlinestring(name = leadstr+'-line')
        ls1 = kml.newpoint(name=leadstr+'-start')
        ls2 = kml.newpoint(name=leadstr+'-end')
        ls3 = kml.newmultigeometry(name=leadstr+'-points')
        
        ls0.style.linestyle.width = 0.5
        ls1.style.iconstyle.scale = 3  # Icon thrice as big
        ls2.style.iconstyle.scale = 3  # Icon thrice as big      
        ls3.style.iconstyle.scale = 0.3 
        
        ls1.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/paddle/'+leadstr[0]+'.png'
        ls0.style.linestyle.color, ls3.style.iconstyle.icon.href, ls2.style.iconstyle.icon.href = kmlFormat(FormatCount)
        ls0.coords = []
        ls1.coords = []
        ls2.coords = []
        ls3.coords = []
        
        lsInitial = 1
        for idx in range(0, len(Lat)):
            ls0.coords.addcoordinates([(Lat[idx],Lon[idx])])
            ls2.coords = [(Lat[idx],Lon[idx])]
            ls3.newpoint(coords= [(Lat[idx],Lon[idx])])
            if (lsInitial == 1):
                lsInitial = 0
                ls1.coords.addcoordinates([(Lat[idx],Lon[idx])])
    kml.save(path+'/'+fileName+'_Package.kml')    