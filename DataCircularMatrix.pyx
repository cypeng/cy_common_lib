# -*- coding: utf-8 -*-
"""
Created on Fri Jun 29 10:26:10 2018

@author: user009
"""
import numpy as np
cimport numpy as np

cdef class DataCircularMatrix():
    cdef:
        int MaxLength
        int count
        int DataNo
        np.ndarray DataMatrix
        
    def __cinit__(self, DataNo):
        self.MaxLength = 5000
        self.count = -1
        self.DataNo= DataNo
        self.DataMatrix = np.ones([self.MaxLength, self.DataNo])*np.nan
    
    cpdef np.ndarray getData(self):
        cpdef np.ndarray DataTemp
        
        DataTemp = self.DataMatrix[0:self.count+1,:]
        self.DataMatrix = np.ones([self.MaxLength, self.DataNo])*np.nan
        self.count = -1
        return DataTemp
    
    cpdef putData(self, np.ndarray newData):
        self.count += 1
        if (self.count == self.MaxLength):
            self.count = 0
            self.DataMatrix = np.ones([self.MaxLength, self.DataNo])*np.nan
        self.DataMatrix[self.count,:] = newData
        
    cpdef int isCountLimit(self):
        if (self.count == self.MaxLength-1):
            return bool(1)
        else:
            if (self.count < 0):
                return -1
            else:
                return bool(0)

# 20180831 C.Y. Peng, String Array Ver for Circulatr Matrix
cdef class StringArrayCircularMatrix():
    cdef:
        int MaxLength
        int count
        np.ndarray StringArray
        
    def __cinit__(self):
        self.MaxLength = 5000
        self.count = -1
        self.StringArray = np.array(['' for _ in range(self.MaxLength)], dtype = object)
    
    cpdef np.ndarray getStringArray(self):
        cpdef np.ndarray DataTemp   
        StringTemp = self.StringArray[0:self.count+1]
        self.StringArray = np.array(['' for _ in range(self.MaxLength)], dtype = object)
        self.count = -1
        return StringTemp
    
    cpdef putString(self, str newString):
        self.count += 1
        if (self.count == self.MaxLength):
            self.count = 0
            self.StringArray = np.array(['' for _ in range(self.MaxLength)], dtype = object)
        self.StringArray[self.count] = newString
        
    cpdef int isCountLimit(self):
        if (self.count == self.MaxLength-1):
            return bool(1)
        else:
            if (self.count < 0):
                return -1
            else:
                return bool(0)        